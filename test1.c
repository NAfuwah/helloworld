#include <stdio.h>
#include <math.h>

int main(){
    float a; float b; float c;
    printf("This code sloves Quadratic equations in the form ax^2 + bx + c = 0\n");
    printf("Enter the value of a\n");
    scanf("%f", &a);

if(a == 0){
    printf("a cannot be 0. Please enter another value.\n");
    return 1;}

    printf("Enter the value of b\n");
    scanf("%f", &b);
    printf("Enter the value of c\n");
    scanf("%f", &c);
    float d = (b * b) - (4 * a * c);

if(d < 0){
    printf("The roots are complex\n");}
else if(d >= 0){
    float e = sqrt(d);
    float x1 = (-b + e) / (2 * a);
    float x2 = (-b - e) / (2 * a);
    printf("x1 is %.f and x2 is %.f\n" ,x1 ,x2);}
    return 0;}